/*
* Necessary compiler libraries:
* Windows: -lWs2_32 -lIphlpapi
*/

#ifndef _ASERVERMANAGER_H
#define _ASERVERMANAGER_H
#include "a3Global.h"

#define localHost 0x7f000001 //127.0.0.1

extern uint32_t hostAddress;
extern uint16_t connectionsMax, connectionCount, connectionPort;

extern void initServers(void);
extern void serverConnect(const uint32_t address, const uint16_t port);
extern uint32_t serverAccept(uint16_t *connectionID);
extern uint8_t serverStart(const uint16_t port, const uint16_t maxConnections);
extern void serverStop(const char *disconnectMessage, const uint32_t length);
extern void serverSendData(const uint16_t connectionID, const char *data, const uint32_t length) __attribute__((nonnull));
extern char *serverGetData(const uint8_t isServer);

#endif
