#ifndef _AFILEMANAGER_H
#define _AFILEMANAGER_H
#include "a3Global.h"
#if defined(__unix__) || defined(MACOS)
	#include <limits.h>
	#include <sys/stat.h>
	#include <unistd.h>
	#define maximumFilePath PATH_MAX
#elif defined(_WIN32)
	#include <windows.h>
	#define maximumFilePath MAX_PATH
#endif

extern char *getWorkingDirectory(void);
extern char *getExecutableDirectory(void);
extern void logData(const char *data, const char *filePath, const uint8_t overwrite) __attribute__((nonnull));
extern char *loadSound(const FILE *file, const uint16_t id) __attribute__((nonnull));
extern char *loadTexture(const FILE *file, const uint16_t id) __attribute__((nonnull));
extern char *loadText(const FILE *file, const uint16_t id) __attribute__((nonnull));

#endif
