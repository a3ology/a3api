#ifndef _ATIMEMANAGER_H
#define _ATIMEMANAGER_H
#include "a3Global.h"

extern void sleepMicros(uint32_t microseconds);
extern void sleepMillis(uint32_t milliseconds);
extern void sleepSecs(uint32_t seconds);
extern char *timeToText(const int32_t time) __attribute__((returns_nonnull));

#endif
