# a3API - LICENSE

----

You may use and distribute this code in any way only under the following conditions:

* This file is included in all distributed products that include a3API without change from the original.

* The credits file (CREDITS.md) is included in all distributed products that include a3API without change from the original.

* The use of the product including a3API violates no other laws.

* The product including a3API does not claim a3API to be its own.

**Violation of these terms will result in prosecution of those responsible to the full extent of the law.**