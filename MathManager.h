#ifndef _AMATH_H_
#define _AMATH_H_
#include "a3Global.h"

//
#define EULER 2.7182818284590452354
#define PI 3.14159265358979323846
//
#define LN2 0.69314718055994530942 //logNatural(2)
#define LN10 2.30258509299404568402 //logNatural(10)
#define LOG2E 1.4426950408889634074 //log2(EULER)
#define LOG10E 0.43429448190325182765 //log10(EULER)
#define PISQRT 1.77245385091 //sqrt(PI)

extern intmax_t roundUp (const float x);
extern intmax_t roundDown (const float x);
/*extern float logarithmNatural(float x);
extern float logarithm(const float base, float x);
extern float logarithm2(intmax_t x);
extern float logarithm10(intmax_t x);*/
extern uint8_t digits10(const intmax_t x);

#endif
