/*
* Necessary compiler libraries:
* Linux: -lpthread
*/

#ifndef _ATHREADMANAGER_H
#define _ATHREADMANAGER_H
#include "a3Global.h"
#if defined(__unix__)
#include <pthread.h>
#elif defined(_WIN32)
#include <windows.h>
#endif

extern void threadCreate(void (*function) (void), uint8_t *threadID) __attribute__((nonnull));
extern void threadClose(const uint8_t threadID);
void threadSetPause(const uint8_t threadID, const uint8_t to);
extern void threadsStop(void);

#endif
