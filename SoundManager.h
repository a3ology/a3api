/*
* Necessary compiler libraries:
* Linux: -lasound
*/

#ifndef _ASOUNDMANAGER_H
#define _ASOUNDMANAGER_H
#include "a3Global.h"

extern void initSounds(void);
extern void playSound(const void *soundData);

#endif
