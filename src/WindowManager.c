#include "../WindowManager.h"
#include <time.h>
#if defined(__unix__)
#include <X11/Xlib.h>
Display *display = null;
Window window = 0, rootWindow = 0;
GC gc = {0};
XImage *frame = null;
XEvent fullscreenEvent = {0};
Cursor cursor = 0;
#elif defined(_WIN32)
#include <windows.h>
HWND window = 0;
WINDOWPLACEMENT windowPosition = {0};
HCURSOR cursor = 0;
HBITMAP frame = {0};
HDC hdc = {0};
BITMAPINFO bmi;
uint8_t windowBorderWidth = 0, windowBorderHeight = 0, windowBorderOffsetX = 0, windowBorderOffsetY = 0;
long fullscreenStyle = 0, fullscreenExStyle = 0;
char *windowTitle = null, *windowIcon = null;
uint8_t windowIconDimensions = 0;
#endif

uint8_t runWindow = 1, startedWindow = 0;
uint16_t screenWidth = 0, screenHeight = 0, maxWidth = 0, maxHeight = 0;
uint16_t windowWidth = 0, windowHeight = 0;
uint16_t windowPosX = 0, windowPosY = 0;
uint32_t *frameData = null;
uint8_t eventThread = 0;

void (*closeOperation) (void);
void (*onWindowResized) (const uint16_t width, const uint16_t height);
void (*onWindowMoved) (const uint16_t posX, const uint16_t posY);
void (*onButtonPress) (const uint16_t button, const uint16_t x, const uint16_t y);
void (*onButtonRelease) (const uint16_t button, const uint16_t x, const uint16_t y);
void (*onPointerMove) (const uint16_t x, const uint16_t y);
void (*onFocusGained) (void);
void (*onFocusLost) (void);

#if defined(_WIN32)
LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam) {
	switch (message) {
		case WM_MOUSEMOVE:;
			if (onPointerMove) (*onPointerMove)(LOWORD(lparam), HIWORD(lparam));
		break;
		case WM_KEYDOWN:;
			POINT positionPointer = {0};
			GetCursorPos(&positionPointer);
			if (onButtonPress) (*onButtonPress)(wparam, positionPointer.x, positionPointer.y);
		break;
		case WM_KEYUP:;
			POINT positionButton = {0};
			GetCursorPos(&positionButton);
			if (onButtonRelease) (*onButtonRelease)(wparam, positionButton.x, positionButton.y);
		break;
		case WM_SETFOCUS:;
			if (!startedWindow) startedWindow = 1;
			if (onFocusGained) (*onFocusGained)();
		break;
		case WM_KILLFOCUS:;
			if (onFocusLost) (*onFocusLost)();
		break;
		case WM_SIZE:;
			windowWidth = LOWORD(lparam);
			windowHeight = HIWORD(lparam);
			if (windowWidth == screenWidth) windowBorderOffsetX = 0;
			else windowBorderOffsetX = windowBorderWidth;
			if (windowHeight == screenHeight) windowBorderOffsetY = 0;
			else windowBorderOffsetY = windowBorderHeight;
			if (onWindowResized) (*onWindowResized)(windowWidth, windowHeight);
		break;
		case WM_MOVE:;
			windowPosX = LOWORD(lparam);
			windowPosY = HIWORD(lparam);
			if (onWindowMoved) (*onWindowMoved)(windowPosX, windowPosY);
		break;
		case WM_CHAR:;
			if (wparam == VK_ESCAPE) DestroyWindow(hwnd);
			break;
		case WM_DESTROY:;
			if (closeOperation) (*closeOperation)();
			PostQuitMessage(0);
		break;
		default:;
			return DefWindowProc(hwnd, message, wparam, lparam);
		break;
	}
	return message;
}
#endif

void windowClose(void) {
	runWindow = 0;
	clock_t start = clock();
	while (startedWindow || clock() - start > 1000000);
	if (frameData) free(frameData);
	#if defined(__unix__)
		if (frame) {
			free(frame);
			frame = null;
		}
		if (cursor) XFreeCursor(display, cursor);
		if (window) XDestroyWindow(display, window);
		if (gc) XFreeGC(display, gc);
		if (display) {
			XCloseDisplay(display);
			display = null;
		}
	#elif defined(_WIN32)
		if (hdc) ReleaseDC(window, hdc);
		if (frame) DeleteObject(frame);
		if (window) DestroyWindow(window);
	#else
		fputs("FATAL: Operating system not supported!\n", stderr);
		abort();
	#endif
}
void eventHandler(void) {
	#if defined(__unix__)
		XEvent event = {0};
		while (runWindow) {
			if (XEventsQueued(display, QueuedAfterFlush)) {
				XNextEvent(display, &event);
				switch (event.type) {
					case MotionNotify:;
						if (onPointerMove) (*onPointerMove)(event.xmotion.x, event.xmotion.y);
					break;
					case ButtonPress:;
						if (onButtonPress) (*onButtonPress)(event.xbutton.button, event.xbutton.x, event.xbutton.y);
					break;
					case ButtonRelease:;
						if (onButtonRelease) (*onButtonRelease)(event.xbutton.button, event.xbutton.x, event.xbutton.y);
					break;
					case KeyPress:;
						if (onButtonPress) (*onButtonPress)((uint8_t) (event.xkey.keycode & 0xff), event.xkey.x, event.xkey.y);
					break;
					case KeyRelease:;
						if (onButtonRelease) (*onButtonRelease)((uint8_t) (event.xkey.keycode & 0xff), event.xkey.x, event.xkey.y);
					break;
					case FocusIn:;
						if (onFocusGained) (*onFocusGained)();
					break;
					case FocusOut:;
						if (onFocusLost) (*onFocusLost)();
					break;
					case ConfigureNotify:;
						if (windowWidth != event.xconfigure.width || windowHeight != event.xconfigure.height) {
							windowWidth = event.xconfigure.width;
							windowHeight = event.xconfigure.height;
							if (onWindowResized) (*onWindowResized)(windowWidth, windowHeight);
						}
						if (windowPosX != event.xconfigure.x || windowPosY != event.xconfigure.y) {
							windowPosX = event.xconfigure.x;
							windowPosY = event.xconfigure.y;
							if (onWindowMoved) (*onWindowMoved)(windowPosX, windowPosY);
						}
					break;
					case MapNotify:;
						if (!startedWindow) startedWindow = 1;
					break;
					case ClientMessage:;
						if ((Atom) event.xclient.data.l[0] == XInternAtom(display, "WM_DELETE_WINDOW", 0)) {
							if (closeOperation) (*closeOperation)();
							return;
						}
					break;
				}
			}
		}
	#elif defined(_WIN32)
		MSG message = {0};
		while (runWindow) {
			if (PeekMessageA(&message, window, 0, 0, PM_REMOVE) > 0) {
				TranslateMessage(&message);
				DispatchMessageA(&message);
			}
		}
	#else
		fputs("FATAL: Operating system not supported!\n", stderr);
		abort();
	#endif
}

void setCloseOperation(void (*operation)) {
	closeOperation = operation;
}
void setWindowResizeOperation(void (*operation) (const uint16_t width, const uint16_t height)) {
	onWindowResized = operation;
}
void setWindowMoveOperation(void (*operation) (const uint16_t posX, const uint16_t posY)) {
	onWindowMoved = operation;
}
void setButtonPressOperation(void (*operation) (const uint16_t button, const uint16_t x, const uint16_t y)) {
	onButtonPress = operation;
}
void setButtonReleaseOperation(void (*operation) (const uint16_t button, const uint16_t x, const uint16_t y)) {
	onButtonRelease = operation;
}
void setPointerMoveOperation(void (*operation) (const uint16_t x, const uint16_t y)) {
	onPointerMove = operation;
}
void setFocusGainedOperation(void (*operation) (void)) {
	onFocusGained = operation;
}
void setFocusLostOperation(void (*operation) (void)) {
	onFocusLost = operation;
}
void windowSetPointerPosition(const uint16_t x, const uint16_t y) {
	#if defined(__unix__)
		XWarpPointer(display, 0, window, 0, 0, windowWidth, windowHeight, x ? x : windowWidth / 2, y ? y : windowHeight / 2);
	#elif defined(_WIN32)
		SetCursorPos(windowPosX + (x ? x : windowWidth / 2), windowPosY + (y ? y : windowHeight / 2));
	#else
		fputs("FATAL: Operating system not supported!\n", stderr);
		abort();
	#endif
}
void windowCapturePointer(void) {
	#if defined(__unix__)
		XGrabPointer(display, window, 1, PointerMotionMask, GrabModeAsync, GrabModeAsync, window, 0, 0);
	#elif defined(_WIN32)
		SetCapture(window);
	#else
		fputs("FATAL: Operating system not supported!\n", stderr);
		abort();
	#endif
}
void windowReleasePointer(void) {
	#if defined(__unix__)
		XUngrabPointer(display, CurrentTime);
	#elif defined(_WIN32)
		ReleaseCapture();
	#else
		fputs("FATAL: Operating system not supported!\n", stderr);
		abort();
	#endif
}
void windowHideCursor(void) {
	#if defined(__unix__)
		XDefineCursor(display, window, cursor);
	#elif defined(_WIN32)
		SetCursor(null);
	#else
		fputs("FATAL: Operating system not supported!\n", stderr);
		abort();
	#endif
}
void windowShowCursor(void) {
	#if defined(__unix__)
		XUndefineCursor(display, window);
	#elif defined(_WIN32)
		SetCursor(cursor);
	#else
		fputs("FATAL: Operating system not supported!\n", stderr);
		abort();
	#endif
}
void windowRaise(void) {
	#if defined(__unix__)
		XMapRaised(display, window);
	#elif defined(_WIN32)
		BringWindowToTop(window);
	#else
		fputs("FATAL: Operating system not supported!\n", stderr);
		abort();
	#endif
}
void windowSetFullscreen(const uint8_t to) {
	#if defined(__unix__)
		fullscreenEvent.xclient.data.l[0] = to;
		XSendEvent(display, rootWindow, 0, SubstructureRedirectMask | SubstructureNotifyMask, &fullscreenEvent);
		XRaiseWindow(display, window);
	#elif defined(_WIN32) //TODO Test
		if (to) {
			fullscreenStyle &= (~WS_BORDER) & (~WS_DLGFRAME) & (~WS_THICKFRAME);
			fullscreenExStyle &= ~WS_EX_WINDOWEDGE;
			GetWindowPlacement(window, &windowPosition);
		}
		else {
			fullscreenStyle &= WS_BORDER & WS_DLGFRAME & WS_THICKFRAME;
			fullscreenExStyle &= WS_EX_WINDOWEDGE;
		}
		SetWindowLongA(window, GWL_STYLE, fullscreenStyle);
		SetWindowLongA(window, GWL_EXSTYLE, fullscreenExStyle);
		ShowWindow(window, to ? SW_SHOWMAXIMIZED : SW_SHOWNORMAL);
		if (!to) SetWindowPlacement(window, &windowPosition);
	#else
		fputs("FATAL: Operating system not supported!\n", stderr);
		abort();
	#endif
}
uint32_t pixelFromRGB(const uint8_t red, const uint8_t green, const uint8_t blue) {
	#if defined(__unix__)
		return (red << 16) | (green << 8) | blue;
	#elif defined(_WIN32)
		return red | (green << 8) | (blue << 16);
	#else
		fputs("FATAL: Operating system not supported!\n", stderr);
		abort();
	#endif
}
#if defined(_WIN32)
void initWindow(void) {
	WNDCLASSA windowClass = {0};
	windowClass.hCursor = null;
	windowClass.hInstance = null;
	if (windowIcon && windowIconDimensions) {
		//TODO Collapse if parsing is not needed
		windowClass.hIcon = CreateIcon(null, (int8_t) windowIconDimensions, (int8_t) windowIconDimensions, 1, 1, (uint8_t *) windowIcon, (uint8_t *) windowIcon); //TODO Check last 3 parameters (probably don't work), make sure icon's 0xaabbccdd per pixel works with Windows's style
	}
	windowClass.lpfnWndProc = WndProc;
	windowClass.lpszClassName = windowTitle;
	windowClass.style = CS_HREDRAW | CS_VREDRAW;
	windowClass.lpszMenuName = windowTitle;
	if (!RegisterClassA(&windowClass)) {
		fputs("FATAL: Could not register window class!\n", stderr);
		abort();
	}
	window = CreateWindowExA(WS_EX_LEFT, windowTitle, windowTitle, WS_OVERLAPPEDWINDOW, windowPosX, windowPosY, windowWidth, windowHeight, null, null, null, null);
	if (!window) {
		fputs("FATAL: Could not create window!\n", stderr);
		abort();
	}
	//TODO Find biggest screen for frameData allocation
	frameData = calloc(ptrSize + maxWidth * maxHeight, 4);
	if (!(hdc = GetWindowDC(window))) {
		fputs("FATAL: Could not get window device context!\n", stderr);
		windowClose();
		abort();
	}
	bmi.bmiHeader.biBitCount = 32;
	bmi.bmiHeader.biPlanes = 1;
	bmi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bmi.bmiHeader.biWidth = maxWidth;
	bmi.bmiHeader.biHeight = maxHeight;
	frame = CreateDIBitmap(hdc, &bmi.bmiHeader, CBM_INIT, frameData, &bmi, DIB_RGB_COLORS);
	if (!frame) {
		fputs("FATAL: Could not create bitmap!\n", stderr);
		windowClose();
		abort();
	}
	GetWindowPlacement(window, &windowPosition);
	windowBorderWidth = GetSystemMetrics(SM_CXSIZEFRAME);
	windowBorderHeight = GetSystemMetrics(SM_CYCAPTION) + windowBorderWidth;
	windowBorderOffsetX = windowBorderWidth;
	windowBorderOffsetY = windowBorderHeight;
	fullscreenStyle = GetWindowLongA(window, GWL_STYLE);
	fullscreenExStyle = GetWindowLongA(window, GWL_EXSTYLE);
	ShowWindow(window, SW_RESTORE);
	eventHandler();
}
#endif
void windowCreate(const uint16_t posX, const uint16_t posY, const uint16_t width, const uint16_t height, const char *title, const uint8_t iconDimensions, const char *icon) {
	#if defined(__unix__)
		if (display) {
			fputs("WARNING: Already opened window!\n", stderr);
			return;
		}
		XInitThreads();
		display = XOpenDisplay(null);
		if (!display) {
			fputs("FATAL: Could not open X display!\n", stderr);
			abort();
		}
		Screen *screen = XDefaultScreenOfDisplay(display);
		screenWidth = screen->width;
		screenHeight = screen->height;
		maxWidth = screenWidth;
		maxHeight = screenHeight;
		if (screenWidth < 256 || screenHeight < 256) {
			fputs("FATAL: Default screen too small for window!\n", stderr);
			windowClose();
			abort();
		}
		windowWidth = width ? width : screenWidth / 1.5;
		windowHeight = height ? height : screenHeight / 1.5;
		windowPosX = posX ? posX : screenWidth / 2 - windowWidth / 2;
		windowPosY = posY ? posY : screenHeight / 2 - windowHeight / 2;
		for(uint8_t screenNumber = XScreenNumberOfScreen(screen); screenNumber != XScreenCount(display); screenNumber++) { //TODO Test with monitor
			if ((screen = XScreenOfDisplay(display, screenNumber))->width > maxWidth) maxWidth = screen->width;
			if (screen->height > maxHeight) maxHeight = screen->height;
		}
		rootWindow = XRootWindowOfScreen(screen);
		Visual *visual = XDefaultVisualOfScreen(screen);
		visual->bits_per_rgb = 24;
		XSetWindowAttributes windowAttributes = {0};
		windowAttributes.event_mask = KeyPressMask | KeyReleaseMask | KeymapStateMask | ButtonPressMask | ButtonReleaseMask | PointerMotionMask | FocusChangeMask | StructureNotifyMask;
		windowAttributes.override_redirect = 1;
		if (iconDimensions && icon) {
			//TODO Load icon
		}
		window = XCreateWindow(display, rootWindow, windowPosX, windowPosY, windowWidth, windowHeight, 0, 24, InputOutput, visual, CWEventMask, &windowAttributes);
		if (!window) {
			fputs("FATAL: Could not create window!\n", stderr);
			windowClose();
			abort();
		}
		Atom closeWindowAtom = XInternAtom(display, "WM_DELETE_WINDOW", 0);
		XSetWMProtocols(display, window, &closeWindowAtom, 1);
		XStoreName(display, window, title);
		XSetIconName(display, window, title);
		fullscreenEvent.type = ClientMessage;
		fullscreenEvent.xclient.send_event = 1;
		fullscreenEvent.xclient.display = display;
		fullscreenEvent.xclient.window = window;
		fullscreenEvent.xclient.message_type = XInternAtom(display, "_NET_WM_STATE", 0);
		fullscreenEvent.xclient.format = 32;
		fullscreenEvent.xclient.data.l[1] = XInternAtom(display, "_NET_WM_STATE_FULLSCREEN", 0);
		{XColor blank = {0};
		Pixmap invisibleCursor = XCreatePixmap(display, window, 1, 1, 1); //Causes error with X11, unable to fix...
		cursor = XCreatePixmapCursor(display, invisibleCursor, invisibleCursor, &blank, &blank, 0, 0);}
		gc = XCreateGC(display, window, 0, null);
		frameData = calloc(ptrSize + maxWidth * maxHeight, 4);
		frame = XCreateImage(display, visual, 24, ZPixmap, 0, (char *) frameData, screenWidth, screenHeight, 32, 0);
		if (!gc) {
			fputs("FATAL: Could not create X graphic context!\n", stderr);
			windowClose();
			abort();
		}
		if (!frame) {
			fputs("FATAL: Could not create X frame!\n", stderr);
			windowClose();
			abort();
		}
		XPutImage(display, window, gc, frame, 0, 0, 0, 0, windowWidth, windowHeight);
		threadCreate(eventHandler, &eventThread);
		XMapRaised(display, window);
		XMoveWindow(display, window, windowPosX, windowPosY);
		while (!startedWindow);
	#elif defined(_WIN32)
		screenWidth = GetSystemMetrics(SM_CXSCREEN);
		screenHeight = GetSystemMetrics(SM_CYSCREEN);
		maxWidth = screenWidth;
		maxHeight = screenHeight;
		windowWidth = width ? width : screenWidth / 1.5;
		windowHeight = height ? height : screenHeight / 1.5;
		windowPosX = posX ? posX : screenWidth / 2 - windowWidth / 2;
		windowPosY = posY ? posY : screenHeight / 2 - windowHeight / 2;
		if (screenWidth < 256 || screenHeight < 256) {
			fputs("FATAL: Default screen too small for window!\n", stderr);
			windowClose();
			abort();
		}
		cursor = LoadCursorA(null, IDC_ARROW);
		uint8_t titleLength = stringLength(title);
		windowTitle = malloc(ptrSize + titleLength + 1);
		memoryCopy(windowTitle, title, titleLength);
		windowTitle[titleLength] = '\0';
		uint8_t iconSize = iconDimensions * 4;
		windowIcon = malloc(ptrSize + iconSize);
		memoryCopy(windowTitle, title, iconSize); //FIXME
		windowIconDimensions = iconDimensions;
		threadCreate(initWindow, &eventThread);
		while (!startedWindow);
	#else
		fputs("FATAL: Operating system not supported!\n", stderr);
		abort();
	#endif
}
void windowDrawFrame(void) {
	#if defined(__unix__)
		XPutImage(display, window, gc, frame, 0, 0, 0, 0, windowWidth, windowHeight);
	#elif defined(_WIN32)
		SetDIBits(hdc, frame, 1, windowHeight, frameData, &bmi, DIB_RGB_COLORS);
		/*SetBitmapBits(frame, maxWidth * maxHeight * 4, frameData);
		PAINTSTRUCT ps = {0};
		HDC paint = BeginPaint(window, &ps);
		HDC source = CreateCompatibleDC(paint);
		SelectObject(source, frame);
		BitBlt(paint, 0, 0, windowWidth, windowHeight, source, 0, 0, SRCCOPY);
		DeleteDC(source);
		EndPaint(window, &ps);*/
	#else
		fputs("FATAL: Operating system not supported!\n", stderr);
		abort();
	#endif
}
void windowPutPixel(const uint16_t x, const uint16_t y, const uint32_t color) {
	#if defined(__unix__)
		frameData[maxWidth * y + x] = color;
	#elif defined(_WIN32)
		frameData[maxWidth * y + x] = color; //FIXME Find first origin pixel or "windowWidth * x" instead of "maxWidth * x" (unsure)
		//SetPixel(hdc, x + windowBorderOffsetX, y + windowBorderOffsetY, (COLORREF) color); //TODO Remove when alternative is found
	#else
		fputs("FATAL: Operating system not supported!\n", stderr);
		abort();
	#endif
}
void windowFinishedLoop(void) {
	if (!runWindow) startedWindow = 0;
}
