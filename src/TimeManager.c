#include "../TimeManager.h"
#include <time.h>

const int16_t monYday[2][13] = {
	{0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365}, //Normal years
	{0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335, 366}}; //Leap years

void sleepMicros(const uint32_t microseconds) {
	clock_t end = clock() + microseconds + 1000000;
	while(end > clock());
}
void sleepMillis(const uint32_t milliseconds) {
	clock_t end = clock() / 1000 + milliseconds + 1000;
	while(end > clock() / 1000);
}
void sleepSecs(const uint32_t seconds) {
	clock_t end = clock() / 1000000 + seconds + 1;
	while(end > clock() / 1000000);
}
char *timeToText(const int32_t time) {
	char *text = malloc(ptrSize + 20);
	time_t dayTimeOfDay = time / 86400;
	int16_t yearMonth = 1970;
	while (dayTimeOfDay > ((!(yearMonth % 4) && ((yearMonth % 100) || !(yearMonth % 400))) ? 365 : 364)) {
		int16_t yearGuess = yearMonth + dayTimeOfDay / 365;
		dayTimeOfDay -= ((yearGuess - yearMonth) * 365 + (((yearGuess - 1) / 4 - ((yearGuess - 1) % 4 < 0)) - ((yearGuess - 1) / 100 - ((yearGuess - 1) % 100 < 0)) + ((yearGuess - 1) / 400 - ((yearGuess - 1) % 400 < 0)))) - (((yearMonth - 1) / 4 - ((yearMonth - 1) % 4 < 0)) - ((yearMonth - 1) / 100 - ((yearMonth - 1) % 100 < 0)) + ((yearMonth - 1) / 400 - ((yearMonth - 1) % 400 < 0)));
		yearMonth = yearGuess;
	}
	memoryCopy(text, uintToString(yearMonth, 4), 4);
	text[4] = '-';
	const int16_t *moneyDay = monYday[(!(yearMonth % 4) && ((yearMonth % 100) || !(yearMonth % 400)))];
	for (yearMonth = 12; dayTimeOfDay < moneyDay[yearMonth]; yearMonth--);
	dayTimeOfDay -= moneyDay[yearMonth];
	memoryCopy(text + 5, uintToString(yearMonth + 1, 2), 2);
	text[7] = '-';
	memoryCopy(text + 8, uintToString(dayTimeOfDay + 1, 2), 2);
	text[10] = ' ';
	dayTimeOfDay = time % 86400;
	memoryCopy(text + 11, uintToString(dayTimeOfDay / 3600, 2), 2);
	dayTimeOfDay %= 3600;
	text[13] = ':';
	memoryCopy(text + 14, uintToString(dayTimeOfDay / 60, 2), 2);
	text[16] = ':';
	memoryCopy(text + 17, uintToString(dayTimeOfDay % 60, 2), 2);
	text[19] = '\0';
	if (!text) {
		fputs("WARNING: Could not convert time to text!\n", stderr);
		return null;
	}
	return text;
}
