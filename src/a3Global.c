#include "../a3Global.h"

void bitSet(uint8_t *byteRange, const uint8_t bit) {
	*byteRange |= 1u << bit;
}
void bitClear(uint8_t *byteRange, const uint8_t bit) {
	*byteRange &= ~(1u << bit);
}
void bitToggle(uint8_t *byteRange, const uint8_t bit) {
	*byteRange ^= 1u << bit;
}
void bitChange(uint8_t *byteRange, const uint8_t bit, const uint8_t to) {
	*byteRange ^= (-to ^ *byteRange) & (1u << bit);
}
uint8_t bitTest(uint8_t byteRange, const uint8_t bit) {
	return (byteRange >> bit) & 1u;
}
char *uintToString(const uintmax_t number, const uint8_t digits) {
	if (digits > 10) return null;
	char *data = malloc(ptrSize + digits + 1);
	uint8_t i = digits;
	if (number == 0) {
		*data = '0';
		*data++ = '\0';
		return data;
	}
	data+= i;
	*data = '\0';
	for (uintmax_t temp = number; temp && i; temp /= 10, i--) {
		data--;
		*data = temp % 10 + '0';
	}
	while (i--) {
		data--;
		*data = '0';
	}
	if (!data) {
		puts("Could not convert int to string!\n");
		abort();
	}
	return data;
}
char *intToString(const intmax_t number, const uint8_t digits) {
	if (digits > 10) return null;
	char *data;
	if (number == 0) {
		data = malloc(ptrSize + 2);
		*data = '0';
		*data++ = '\0';
		return data;
	}
	if (number < 0) {
		data = malloc(ptrSize + digits + 2);
		*data = '-';
	}
	else data = malloc(ptrSize + digits + 1);
	uint8_t i = digits;
	data+= i;
	*data = '\0';
	for (intmax_t temp = number; temp && i; temp /= 10, i--) {
		data--;
		*data = temp % 10 + '0';
	}
	while (i--) {
		data--;
		*data = '0';
	}
	if (!data) {
		puts("Could not convert int to string!\n");
		abort();
	}
	return data;
}
uintmax_t stringLength(const char *string) {
	uintmax_t length = 1;
	for (;length && *string++ != '\0'; length++);
	return length - 1;
}
uint32_t memoryIndexReverse(const void *source, const uint32_t size, const uint8_t character) {
	const uint8_t *sourceData = source;
	uint32_t i = size;
	sourceData += i;
	while (i-- && *sourceData-- != character);
	return i;
}
uint32_t memoryIndexIReverse(const void *source, const uint32_t size, const uint8_t character, const uint8_t indecies) {
	const uint8_t *sourceData = source;
	uint32_t i = size;
	sourceData += i;
	uint8_t count = 0;
	while (i-- && count != indecies) if(*sourceData-- == character) count++;
	return i;
}
uint32_t memoryIndex(const void *source, const uint32_t size, const uint8_t character) {
	const uint8_t *sourceData = source;
	uint16_t rIndex = size;
	while (rIndex-- && *sourceData++ != character);
	return size - rIndex;
}
uint32_t memoryIndexI(const void *source, const uint32_t size, const uint8_t character, const uint8_t indecies) {
	const uint8_t *sourceData = source;
	uint16_t rIndex = 0;
	uint8_t count = 0;
	while (rIndex-- && count != indecies) if(*sourceData == character) count++;
	return size - rIndex;
}
void *memoryCopy(void *destination, const void *source, const uintmax_t size) {
	uint8_t *destinationData = destination;
	const uint8_t *sourceData = source;
	uintmax_t i = size;
	while (i--) *destinationData++ = *sourceData++;
	return destination;
}
uint8_t memoryEquals(const void *source, const void *against, const uintmax_t size) {
	const uint8_t *sourceData = source, *againstData = against;
	uintmax_t i = size;
	while (i--) if (*sourceData++ != *againstData++) return 0;
	return 1;
}
void *memoryClear(void *destination, const uintmax_t size) {
	uint8_t *destinationData = destination;
	while (size--) *destinationData++ = 0;
	return destination;
}
void *memorySet(void *destination, const uintmax_t size, const char to) {
	uint8_t *destinationData = destination;
	uintmax_t i = size;
	while (i--) *destinationData++ = to;
	return destination;
}
uint16_t hostToBigEndian16(uint16_t input) {
	#if BYTE_ORDER == LITTLE_ENDIAN
		input = (input << 8) | (input >> 8);
	#endif
	return input;
}
uint32_t hostToBigEndian32(uint32_t input) {
	#if BYTE_ORDER == LITTLE_ENDIAN
		input = ((input >> 24) & 0xff) | ((input << 8) & 0xff0000) | ((input >> 8) & 0xff00) | ((input << 24) & 0xff000000);
	#endif
	return input;
}
uint16_t hostToLittleEndian16(uint16_t input) {
	#if BYTE_ORDER == BIG_ENDIAN
		input = (input << 8) | (input >> 8);
	#endif
	return input;
}
uint32_t hostToLittleEndian32(uint32_t input) {
	#if BYTE_ORDER == BIG_ENDIAN
	input = ((input >> 24) & 0xff) | ((input << 8) & 0xff0000) | ((input >> 8) & 0xff00) | ((input << 24) & 0xff000000);
	#endif
	return input;
}
