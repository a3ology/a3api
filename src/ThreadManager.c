#include "../ThreadManager.h"
#if defined(__unix__)
#include <pthread.h>
#elif defined(_WIN32)
#include <windows.h>
#endif

uint8_t threadCount = 0;
void **threads = null;
void threadCreate(void (*function) (void), uint8_t *threadID) {
	*threadID = threadCount;
	threadCount++;
	threads = realloc(threads, ptrSize + threadCount * 4);
	#if defined(__unix__)
		if (pthread_create((pthread_t *) &(threads[(*threadID) * 4]), null, (void *) (void *) function, null)) {
			fputs("FATAL: Could not create thread!\n", stderr);
			abort();
		}
	#elif defined(_WIN32)
		uint32_t tempID = 0;
		threads[(*threadID) * 4] = CreateThread(null, 0, (LPTHREAD_START_ROUTINE) function, 0, 0, (PDWORD) &tempID);
		if (!threads[(*threadID) * 4]) {
			fputs("FATAL: Could not create thread!\n", stderr);
			abort();
		}
	#else
		fputs("FATAL: Operating system not supported!\n", stderr);
		abort();
	#endif
}
void threadClose(const uint8_t threadID) {
	if (threadID > threadCount - 1) {
		fputs("FATAL: Invalid thread ID!\n", stderr);
		abort();
	}
	#if defined(__unix__)
		pthread_cancel((pthread_t) threads[threadID]);
		for (uint8_t index = threadID; index != threadCount - 1; index++) threads[index] = threads[index + 1];
		threads = realloc(threads, ptrSize + threadCount * 4);
	#elif defined(_WIN32)
		TerminateThread(threads[(int) threadID], 0); //TODO Check
		for (uint8_t index = threadID; index != threadCount - 1; index++) threads[index] = threads[index + 1];
		threads = realloc(threads, ptrSize + threadCount * 4);
	#else
		fputs("FATAL: Operating system not supported!\n", stderr);
		abort();
	#endif
}
void threadSetPause(const uint8_t threadID, const uint8_t to) {
	#if defined(__unix__)
		//TODO
	#elif defined(_WIN32)
		//TODO
	#else
		fputs("FATAL: Operating system not supported!\n", stderr);
		abort();
	#endif
}
void threadsStop(void) {
	if (threads) {
		for (uint8_t index = 0; index != threadCount; index++) threadClose(index);
		free(threads);
		threads = null;
	}
}
