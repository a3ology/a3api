#include "../BigNumberManager.h"

uint8_t *bigNumberSet(const char *numberString, uint16_t *resultSize) {
	const uint8_t digitsBase10 = stringLength(numberString);
	*resultSize = roundUp(digitsBase10 * base10to256Ratio);
	uint8_t *result = malloc(ptrSize + *resultSize);
	//TODO
	return result;
}
uint8_t *bigNumberAdd(const uint8_t *value1, const uint16_t value1Size, const uint8_t *value2, const uint16_t value2Size, uint16_t *resultSize) {
	const uint16_t greatestSize = value1Size > value2Size ? value1Size : value2Size;
	*resultSize = greatestSize;
	uint8_t *result = calloc(ptrSize + greatestSize + 1, 1);
	uint8_t carry = 0;
	for (uint16_t index = 0; index != greatestSize; index++) {
		for (uint8_t bitIndex = 0; bitIndex != 8; bitIndex++) {
			uint8_t value1Bit = (index < value1Size ? bitTest(value1[value1Size - index - 1], bitIndex) : 0), value2Bit = (index < value2Size ? bitTest(value2[value2Size - index - 1], bitIndex) : 0);
			bitChange(&result[greatestSize - index], bitIndex, carry ^ (value1Bit ^ value2Bit)); //carry ? !(value1Bit ^ value2Bit) : value1Bit ^ value2Bit)
			carry = carry ? value1Bit | value2Bit : value1Bit & value2Bit;
		}
	}
	if (carry) {
		result[0] = 0x01;
		*resultSize += 1;
	}
	else {
		memoryCopy(result, result + 1, *resultSize);
		result = realloc(result, ptrSize + greatestSize);
	}
	return result;
}
uint8_t *bigNumberSubtract(const uint8_t *value1, const uint16_t value1Size, const uint8_t *value2, const uint16_t value2Size, uint16_t *resultSize) { //value1 - value2
	const uint16_t greatestSize = value1Size > value2Size ? value1Size : value2Size;
	*resultSize = greatestSize;
	uint8_t *result = malloc(ptrSize + greatestSize);
	uint8_t borrow = 0;
	uint16_t zeroIndex = 0;
	for (uint16_t index = 0; index != greatestSize; index++) {
		for (uint8_t bitIndex = 0; bitIndex != 8; bitIndex++) {
			uint8_t value1Bit = (index < value1Size ? bitTest(value1[value1Size - index - 1], bitIndex) : 0), value2Bit = (index < value2Size ? bitTest(value2[value2Size - index - 1], bitIndex) : 0);
			bitChange(&result[greatestSize - index - 1], bitIndex, borrow ^ (value1Bit ^ value2Bit)); //borrow ? !(value1Bit ^ value2Bit) : value1Bit ^ value2Bit
			borrow = borrow ? (!(value1Bit ^ value2Bit)) | value2Bit : (!value1Bit) & value2Bit;
		}
		if (!result[index]) zeroIndex = index + 1;
		else zeroIndex = 0;
	}
	if (zeroIndex) {
		*resultSize = greatestSize - zeroIndex;
		memoryCopy(result, result + zeroIndex, *resultSize);
		result = realloc(result, ptrSize + *resultSize);
	}
	if (borrow) { //Negative number; error
		result = realloc(result, ptrSize + 1);
		result[0] = 0;
	}
	return result;
}
/*uint8_t *bigNumberMultiply(const uint8_t *value1, const uint16_t value1Size, const uint8_t *value2, const uint16_t value2Size, uint16_t *resultSize) {
	uint8_t *result = malloc(ptrSize + 1);
	return result;
}
uint8_t *bigNumberDivide(const uint8_t *value1, const uint16_t value1Size, const uint8_t *value2, const uint16_t value2Size, uint16_t *resultSize) {
	uint8_t *result = malloc(ptrSize + 1);
	return result;
}*/
uint8_t *bigNumberAND(const uint8_t *value1, const uint16_t value1Size, const uint8_t *value2, const uint16_t value2Size, uint16_t *resultSize) {
	const uint16_t smallestSize = value1Size < value2Size ? value1Size : value2Size;
	*resultSize = smallestSize;
	uint8_t *result = malloc(ptrSize + smallestSize);
	uint16_t zeroIndex = 1;
	for (uint16_t index = 0; index != smallestSize; index++) {
		result[index] = value1[index] & value2[index];
		if (zeroIndex && !result[index]) zeroIndex = index + 1;
		else zeroIndex = 0;
	}
	if (zeroIndex) {
		*resultSize = smallestSize - zeroIndex;
		memoryCopy(result, result + zeroIndex, *resultSize);
		result = realloc(result, ptrSize + *resultSize);
	}
	return result;
}
uint8_t *bigNumberOR(const uint8_t *value1, const uint16_t value1Size, const uint8_t *value2, const uint16_t value2Size, uint16_t *resultSize) {
	*resultSize = value1Size > value2Size ? value1Size : value2Size;
	uint8_t *result = malloc(ptrSize + *resultSize);
	for (uint16_t index = 0; index != *resultSize; index++) result[index] = (index < value1Size ? value1[index] : 0) | (index < value2Size ? value2[index] : 0);
	return result;
}
uint8_t *bigNumberXOR(const uint8_t *value1, const uint16_t value1Size, const uint8_t *value2, const uint16_t value2Size, uint16_t *resultSize) {
	const uint16_t greatestSize = value1Size > value2Size ? value1Size : value2Size;
	*resultSize = greatestSize;
	uint8_t *result = malloc(ptrSize + greatestSize);
	uint16_t zeroIndex = 1;
	for (uint16_t index = 0; index < greatestSize; index++) {
		result[index] = (index < value1Size ? value1[index] : 0) ^ (index < value2Size ? value2[index] : 0);
		if (zeroIndex && !result[index]) zeroIndex = index + 1;
		else zeroIndex = 0;
	}
	if (zeroIndex) {
		*resultSize = greatestSize - zeroIndex;
		memoryCopy(result, result + zeroIndex, *resultSize);
		result = realloc(result, ptrSize + *resultSize);
	}
	return result;
}
