#include "../ServerManager.h"
#if defined(__unix__)
#include <ifaddrs.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#elif defined(_WIN32)
#include <windows.h>
#if _WIN32_WINNT <= _WIN32_WINNT_WIN2K
#undef _WIN32_WINNT
#define _WIN32_WINNT _WIN32_WINNT_WINXP
#endif
#include <iphlpapi.h>
#endif

uint16_t connectionsMax = 0, connectionCount = 0, connectionPort = 0;
uint32_t hostAddress = 0;
uint32_t *connections = null;
int serverSocket = 0, clientSocket = 0;

void initServers(void) {
	clientSocket = socket(AF_INET, SOCK_DGRAM, 0);
	#if defined(__unix__)
		struct ifaddrs *address = null;
		if (getifaddrs(&address) == -1) {
			fputs("FATAL: Could not get ifaddrs list!\n", stderr);
			abort();
		}
		struct ifaddrs *temp = address;
		while (temp) {
			if (temp->ifa_addr->sa_family == AF_INET) {
				uint32_t tempAddress = ((struct sockaddr_in *) temp->ifa_addr)->sin_addr.s_addr;
				if (tempAddress != hostToBigEndian32(localHost)) {
					hostAddress = tempAddress;
					temp = null;
					free(address);
					address = null;
				}
			}
			if (temp) temp = temp->ifa_next;
		}
	#elif defined(_WIN32)
		/*TODO
		uint32_t length = sizeof(IP_ADAPTER_ADDRESSES);
		PIP_ADAPTER_ADDRESSES addresses = malloc(ptrSize + length);
		GetAdaptersAddresses(AF_INET, 0, null, address, &length);*/
	#endif
	if (!hostAddress) {
		fputs("WARNING: Could not get host IP address! Check your connection.\n", stderr);
		return;
	}
}
void serverConnect(const uint32_t address, const uint16_t port) {
	struct sockaddr_in to = {0};
	to.sin_family = AF_INET;
	to.sin_addr.s_addr = address;
	to.sin_port = port;
	if (connect(clientSocket, (struct sockaddr *) &to, sizeof(struct sockaddr)) == -1) {
		fputs("WARNING: Could not connect to server!\n", stderr);
		abort();
	}
}
uint32_t serverAccept(uint16_t *connectionID) {
	uint32_t length = sizeof(struct sockaddr_in);
	struct sockaddr_in connection = {0};
	if (connectionCount != connectionsMax) {
		connections[connectionCount] = accept(serverSocket, (struct sockaddr *) &connection, &length);
		connectionCount++;
		*connectionID = connectionCount;
		return (uint32_t) connection.sin_addr.s_addr;
	}
	return 0;
}
uint8_t serverStart(const uint16_t port, const uint16_t maxConnections) {
	connectionsMax = maxConnections;
	connectionPort = port;
	connections = malloc(ptrSize + maxConnections * 4);
	serverSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (!serverSocket) {
		fputs("Could not open server socket!\n", stderr);
		abort();
	}
	struct sockaddr_in serverAddress = {0};
	{char optval = 1;
	setsockopt(serverSocket, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(int));}
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_port = hostToBigEndian16(port);
	serverAddress.sin_addr.s_addr = hostAddress;
	if (bind(serverSocket, (struct sockaddr *) &serverAddress, sizeof(struct sockaddr)) == -1) {
		fputs("WARNING: Could not bind server socket!\n", stderr);
		return 0;
	}
	if (listen(serverSocket, maxConnections ? (int) maxConnections : (int) 0xffff) == -1) {
		fputs("WARNING: Could not listen on server socket!\n", stderr);
		return 0;
	}
	return 1;
}
void serverSendData(const uint16_t connectionID, const char *data, const uint32_t length) {
	char *sentData = malloc(ptrSize + length + 6);
	sentData[0] = hostAddress & 0xff;
	sentData[1] = (hostAddress >> 8) & 0xff;
	sentData[2] = (hostAddress >> 16) & 0xff;
	sentData[3] = (hostAddress >> 24) & 0xff;
	sentData[4] = ':';
	memoryCopy(sentData + 5, data, length);
	sentData[length + 5] = '\0';
	fputs(sentData, stdout);
	if (connectionID) {
		struct sockaddr_in to = {0};
		to.sin_family = AF_INET;
		to.sin_addr.s_addr = connections[connectionID - 1];
		to.sin_port = connectionPort;
		sendto(clientSocket, sentData, length + 6, 0, (struct sockaddr *) &to, sizeof(struct sockaddr));
	}
	else send(serverSocket, sentData, length + 6, 0);
	free(sentData);
}
char *serverGetData(const uint8_t isServer) {
	char *data = malloc(ptrSize + 0xfffffffe);
	uint32_t length = recv(isServer ? clientSocket : serverSocket, data, 0xfffffffe, 0);
	data[length] = '\0';
	return data;
}
void serverStop(const char *disconnectMessage, const uint32_t length) {
	if (disconnectMessage && connectionCount) for (uint32_t index = 1; index != connectionCount + 1; index++) serverSendData(index, disconnectMessage, length);
	if (serverSocket) shutdown(serverSocket, 2);
	if (clientSocket) shutdown(clientSocket, 2);
	if (connections) {
		connectionCount = 0;
		free(connections);
		connections = null;
	}
}
