#include "../FileManager.h"
#include <errno.h>
#include <stdio.h>
extern int errno;

char *getWorkingDirectory(void) {
	char *directory = calloc(ptrSize + maximumFilePath + 1, 1);
	#if defined(__unix__) || defined(MACOS)
		getcwd(directory, maximumFilePath);
	#elif defined(_WIN32)
		GetCurrentDirectory(maximumFilePath, directory);
	#else
		fputs("FATAL: Operating system not supported!\n", stderr);
		abort();
	#endif
	uint8_t directoryLength = stringLength(directory);
	directory[directoryLength] = '/';
	if (!directory) {
		fputs("WARNING: Could not get executable directory!\n", stderr);
		return null;
	}
	directory = realloc(directory, ptrSize + directoryLength + 2);
	return directory;
}
char *getExecutableDirectory(void) {
	char *directory = calloc(ptrSize + maximumFilePath + 1, 1);
	#if defined(__unix__)
		#if defined(__linux__)
			readlink("/proc/self/exe", directory, maximumFilePath);
		#elif defined(__bsdi__)
			readlink("/proc/curproc/file", directory, maximumFilePath);
		#else
			fputs("FATAL: Operating system not supported!\n", stderr);
			abort();
		#endif
	#elif defined(_WIN32)
		GetModuleFileName(null, directory, maximumFilePath);
	#elif defined(MACOS)
		uint8_t bufferSize = maximumFilePath;
		_NSGetExecutablePath(directory, &bufferSize);
	#else
		fputs("FATAL: Operating system not supported!\n", stderr);
		abort();
	#endif
	uint8_t index = memoryIndexReverse(directory, stringLength(directory), '/');
	directory[index + 1] = '/';
	directory[index + 2] = '\0';
	if (!directory) {
		fputs("FATAL: Could not get executable directory!\n", stderr);
		return null;
	}
	directory = realloc(directory, ptrSize + index + 2);
	return directory;
}
void logData(const char *data, const char *filePath, const uint8_t overwrite) {
	FILE *file = null;
	if (overwrite) file = fopen(filePath, "wb");
	else file = fopen(filePath, "ab");
	if (!file) {
		uint8_t index = memoryIndexReverse(filePath, stringLength(filePath), '/');
		char *directory = malloc(ptrSize + index + 1);
		memoryCopy(directory, filePath, index);
		#if defined(__unix__) || defined(MACOS)
			mkdir(directory, 755);
		#elif defined(_WIN32)
			CreateDirectory(directory, null);
		#else
			fputs("FATAL: Operating system not supported!\n", stderr);
			abort();
		#endif
		free(directory);
		file = fopen(filePath, "wb");
		if (!file) {
			fputs("FATAL: Could not create log file or directory!\n", stderr);
			abort();
		}
		errno = 0;
	}
	fputs(data, file);
	fclose(file);
}
char *loadSound(const FILE *file, const uint16_t id) {
	fseek(file, id, SEEK_START); //TODO
}
char *loadTexture(const FILE *file, const uint16_t id) {
	
}
char *loadText(const FILE *file, const uint16_t id) {
	
}
