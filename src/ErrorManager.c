#include "../ErrorManager.h"
#include <errno.h>
#if defined(__unix__)
#include <sys/signal.h>
#elif defined(_WIN32)
#include <signal.h>
#endif

void initCrashHandler(void (*exitHandlerFunction) (void)) {
	atexit(exitHandlerFunction);
	signal(SIGINT, SIG_IGN);
	signal(SIGILL, SIG_IGN);
	signal(SIGFPE, SIG_IGN);
	signal(SIGSEGV, SIG_IGN);
}
char *getStackTrace(const uint8_t count) {
	//TODO
	#if defined(__unix__)
	#elif defined(_WIN32)
	#else
		fputs("FATAL: Operating system not supported!\n", stderr);
		abort();
	#endif
	return null;
}
char *getError(const int32_t errorCode) {
	char *error = null;
	if (errorCode) {
		uint8_t length = 0;
		switch (errorCode) {
			//Signal errors
			case SIGINT + 0xff:
				length = 31;
				error = malloc(ptrSize + 31);
				memoryCopy(error, "Externally force-closed program", 31);
			break;
			case SIGILL + 0xff:
				length = 19;
				error = malloc(ptrSize + 19);
				memoryCopy(error, "Illegal instruction", 19);
			break;
			case SIGFPE + 0xff:
				length = 30;
				error = malloc(ptrSize + 30);
				memoryCopy(error, "Erroneous arithmetic operation", 30);
			break;
			case SIGSEGV + 0xff:
				length = 25;
				error = malloc(ptrSize + 25);
				memoryCopy(error, "Invalid access to storage", 25);
			break;
			case SIGTERM + 0xff:
				length = 29;
				error = malloc(ptrSize + 29);
				memoryCopy(error, "Program forcefully terminated", 29);
			break;

			//Standard errors
			case EPERM:
				length = 38;
				error = malloc(ptrSize + 38);
				memoryCopy(error, "Operation not permitted; no permission", 38);
			break;
			case ENOENT:
				length = 14;
				error = malloc(ptrSize + 14);
				memoryCopy(error, "File not found", 14);
			break;
			case ESRCH:
				length = 27;
				error = malloc(ptrSize + 27);
				memoryCopy(error, "Specified process not found", 27);
			break;
			case EINTR:
				length = 23;
				error = malloc(ptrSize + 23);
				memoryCopy(error, "Interrupted system call", 23);
			break;
			case EIO:
				length = 24;
				error = malloc(ptrSize + 24);
				memoryCopy(error, "File input/output error", 24);
			break;
			case ENXIO:
				length = 37;
				error = malloc(ptrSize + 37);
				memoryCopy(error, "Specified address or device not found", 37);
			break;
			case E2BIG:
				length = 18;
				error = malloc(ptrSize + 18);
				memoryCopy(error, "Too many arguments", 18);
			break;
			case ENOEXEC:
				length = 23;
				error = malloc(ptrSize + 23);
				memoryCopy(error, "Executable format error", 23);
			break;
			case EBADF:
				length = 19;
				error = malloc(ptrSize + 19);
				memoryCopy(error, "Invalid file number", 19);
			break;
			case EAGAIN:
				length = 15;
				error = malloc(ptrSize + 15);
				memoryCopy(error, "No process slot", 15);
			break;
			case ENOMEM:
				length = 17;
				error = malloc(ptrSize + 17);
				memoryCopy(error, "Ran out of memory", 17);
			break;
			case EACCES:
				length = 24;
				error = malloc(ptrSize + 24);
				memoryCopy(error, "Access permission denied", 24);
			break;
			case EFAULT:
				length = 15;
				error = malloc(ptrSize + 15);
				memoryCopy(error, "Invalid address", 15);
			break;
			case EBUSY:
				length = 24;
				error = malloc(ptrSize + 24);
				memoryCopy(error, "Requested device is busy", 24);
			break;
			case EEXIST:
				length = 19;
				error = malloc(ptrSize + 19);
				memoryCopy(error, "File already exists", 19);
			break;
			case EXDEV:
				length = 25;
				error = malloc(ptrSize + 25);
				memoryCopy(error, "Cross-device interference", 25);
			break;
			case ENODEV:
				length = 26;
				error = malloc(ptrSize + 26);
				memoryCopy(error, "Specified device not found", 26);
			break;
			case ENOTDIR:
				length = 33;
				error = malloc(ptrSize + 33);
				memoryCopy(error, "Target is a file, not a directory", 33);
			break;
			case EISDIR:
				length = 33;
				error = malloc(ptrSize + 33);
				memoryCopy(error, "Target is a directory, not a file", 33);
			break;
			case EINVAL:
				length = 16;
				error = malloc(ptrSize + 16);
				memoryCopy(error, "Invalid argument", 16);
			break;
			case ENFILE:
				length = 19;
				error = malloc(ptrSize + 19);
				memoryCopy(error, "File table overflow", 19);
			break;
			case EMFILE:
				length = 19;
				error = malloc(ptrSize + 19);
				memoryCopy(error, "Too many open files", 19);
			break;
			case EFBIG:
				length = 24;
				error = malloc(ptrSize + 24);
				memoryCopy(error, "File too large to access", 24);
			break;
			case ENOSPC:
				length = 25;
				error = malloc(ptrSize + 25);
				memoryCopy(error, "Computer ran out of space", 25);
			break;
			case ESPIPE:
				length = 17;
				error = malloc(ptrSize + 17);
				memoryCopy(error, "Illegal file seek", 17);
			break;
			case EROFS:
				length = 24;
				error = malloc(ptrSize + 24);
				memoryCopy(error, "File system is read-only", 24);
			break;
			case EMLINK:
				length = 14;
				error = malloc(ptrSize + 14);
				memoryCopy(error, "Too many links", 14);
			break;
			case EDOM:
				length = 28;
				error = malloc(ptrSize + 28);
				memoryCopy(error, "Math operation out of domain", 28);
			break;
			case ERANGE:
				length = 31;
				error = malloc(ptrSize + 31);
				memoryCopy(error, "Math operation result too large", 31);
			break;
			case EILSEQ:
				length = 21;
				error = malloc(ptrSize + 21);
				memoryCopy(error, "Invalid byte sequence", 21);
			break;
			case EDEADLOCK:
				length = 40;
				error = malloc(ptrSize + 40);
				memoryCopy(error, "Continuing would cause resource deadlock", 40);
			break;
			case ENAMETOOLONG:
				length = 18;
				error = malloc(ptrSize + 18);
				memoryCopy(error, "File name too long", 18);
			break;
			case ENOSYS:
				length = 19;
				error = malloc(ptrSize + 19);
				memoryCopy(error, "Invalid system call", 19);
			break;
			case ENOTEMPTY:
				length = 22;
				error = malloc(ptrSize + 22);
				memoryCopy(error, "Directory is not empty", 22);
			break;
			#if defined(__unix__)
			case ELOOP:
				length = 23;
				error = malloc(ptrSize + 23);
				memoryCopy(error, "Too many symbolic links", 23);
			break;
			case ENOSTR:
				length = 22;
				error = malloc(ptrSize + 22);
				memoryCopy(error, "Device is not a stream", 22);
			break;
			case ETIME:
				length = 20;
				error = malloc(ptrSize + 20);
				memoryCopy(error, "Timer/stream expired", 20);
			break;
			case ENOSR:
				length = 23;
				error = malloc(ptrSize + 23);
				memoryCopy(error, "Out of stream resources", 23);
			break;
			case ENOLINK:
				length = 30;
				error = malloc(ptrSize + 30);
				memoryCopy(error, "Link has been cut unexpectedly", 30);
			break;
			case EPROTO:
				length = 14;
				error = malloc(ptrSize + 14);
				memoryCopy(error, "Protocol error", 14);
			break;
			case EOVERFLOW:
				length = 34;
				error = malloc(ptrSize + 34);
				memoryCopy(error, "Value too large for specified type", 34);
			break;
			case ENOBUFS:
				length = 15;
				error = malloc(ptrSize + 15);
				memoryCopy(error, "No buffer space", 15);
			break;
			#endif

			default:
				length = 18;
				error = malloc(ptrSize + 18);
				memoryCopy(error, "Unknown error code", 18);
			break;
		}
		if (!error) {
			fputs("WARNING: Could not set error message!\n", stderr);
			return null;
		}
		uint8_t digits = digits10(errorCode);
		error = realloc(error, length + digits + 16);
		memoryCopy(error + length, "; Error code = ",  15);
		memoryCopy(error + length + 15, intToString(errorCode, digits), digits);
		error[length + digits + 15] = '\0';
	}
	return error;
}
