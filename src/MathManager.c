#include "../MathManager.h"

intmax_t roundUp(const float x) {
	return (intmax_t) x == x ? (intmax_t) x : (intmax_t) x + 1;
}
intmax_t roundDown(const float x) {
	return x >= 0 ? (intmax_t) x : ((intmax_t) x == x ? (intmax_t) x : (intmax_t) x - 1);
}
float logarithmNatural(float x) { //TODO
	return 0;
}
float logarithm(const float base, float x) {
	return logarithmNatural(x) / logarithmNatural(base);
}
float logarithm2(intmax_t x) { //TODO
	return 0;
}
float logarithm10(float x) { //TODO
	return 0;
}
uint8_t digits10(const intmax_t x) {
	float y = (float) x, i = 1;
	if (x > 0) while ((y /= 10) >= 10) i++;
	else if (x < 0) while ((y /= 10) <= -10) i++;
	else return 0;
	if (y >= 1) i++;
	return i;
}
