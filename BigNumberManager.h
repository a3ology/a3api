#ifndef _ABIGNUMBERMANAGER_H
#define _ABIGNUMBERMANAGER_H
#include "a3Global.h"

#define base10to256Ratio 0.415241011861

extern uint8_t *bigNumberSet(const char *numberString, uint16_t *resultSize) __attribute__((nonnull));
extern uint8_t *bigNumberAdd(const uint8_t *value1, const uint16_t value1Size, const uint8_t *value2, const uint16_t value2Size, uint16_t *resultSize) __attribute__((nonnull));
extern uint8_t *bigNumberSubtract(const uint8_t *value1, const uint16_t value1Size, const uint8_t *value2, const uint16_t value2Size, uint16_t *resultSize) __attribute__((nonnull)); //value1 - value2
/*extern uint8_t *bigNumberMultiply(const uint8_t *value1, const uint16_t value1Size, const uint8_t *value2, const uint16_t value2Size, uint16_t *resultSize) __attribute__((nonnull));
extern uint8_t *bigNumberDivide(const uint8_t *value1, const uint16_t value1Size, const uint8_t *value2, const uint16_t value2Size, uint16_t *resultSize) __attribute__((nonnull));*/
extern uint8_t *bigNumberAND(const uint8_t *value1, const uint16_t value1Size, const uint8_t *value2, const uint16_t value2Size, uint16_t *resultSize) __attribute__((nonnull));
extern uint8_t *bigNumberOR(const uint8_t *value1, const uint16_t value1Size, const uint8_t *value2, const uint16_t value2Size, uint16_t *resultSize) __attribute__((nonnull));
extern uint8_t *bigNumberXOR(const uint8_t *value1, const uint16_t value1Size, const uint8_t *value2, const uint16_t value2Size, uint16_t *resultSize) __attribute__((nonnull));

#endif
