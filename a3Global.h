#ifndef _a3GLOBAL_H
#define _a3GLOBAL_H
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#if(defined(__APPLE__) && defined(__MACH__)) || defined(__APPLE_API_STANDARD)
#define MACOS
#endif
#define null ((void *) 0)
#define ptrSize sizeof(uintptr_t)

extern void bitSet(uint8_t *byteRange, const uint8_t bit) __attribute__((nonnull));
extern void bitClear(uint8_t *byteRange, const uint8_t bit) __attribute__((nonnull));
extern void bitToggle(uint8_t *byteRange, const uint8_t bit) __attribute__((nonnull));
extern void bitChange(uint8_t *byteRange, const uint8_t bit, const uint8_t to) __attribute__((nonnull));
extern uint8_t bitTest(uint8_t byteRange, const uint8_t bit);
extern char *uintToString(const uintmax_t number, const uint8_t digits);
extern char *intToString(const intmax_t number, const uint8_t digits);
extern uintmax_t stringLength(const char *string) __attribute__((nonnull));
extern uint32_t memoryIndexReverse(const void *source, const uint32_t size, const uint8_t character) __attribute__((nonnull));
extern uint32_t memoryIndexIReverse(const void *source, const uint32_t size, const uint8_t character, const uint8_t indecies) __attribute__((nonnull));
extern uint32_t memoryIndex(const void *source, const uint32_t size, const uint8_t character) __attribute__((nonnull));
extern uint32_t memoryIndexI(const void *source, const uint32_t size, const uint8_t character, const uint8_t indecies) __attribute__((nonnull));
extern void *memoryCopy(void *destination, const void *source, const uintmax_t size) __attribute__((nonnull)) __attribute__((returns_nonnull));
extern uint8_t memoryEquals(const void *source, const void *against, const uintmax_t size) __attribute__((nonnull));
extern void *memoryClear(void *destination, const uintmax_t size) __attribute__((nonnull)) __attribute__((returns_nonnull));
extern void *memorySet(void *destination, const uintmax_t size, const char to) __attribute__((nonnull)) __attribute__((returns_nonnull));
extern uint16_t hostToBigEndian16(uint16_t input) __attribute__((nonnull));
extern uint32_t hostToBigEndian32(uint32_t input) __attribute__((nonnull));
extern uint16_t hostToLittleEndian16(uint16_t input) __attribute__((nonnull));
extern uint32_t hostToLittleEndian32(uint32_t input) __attribute__((nonnull));

#endif
