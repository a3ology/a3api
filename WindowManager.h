/*
* Necessary compiler libraries:
* Windows: -lgdi32
* Unix: -lX11, -lpthread
*/

#ifndef _AWINDOWMANAGER_H
#define _AWINDOWMANAGER_H
#include "a3Global.h"
#include "ThreadManager.h"
#define PIXEL_WHITE 0xffffff
#define PIXEL_GREY 0x7f7f7f
#define PIXEL_BLACK 0x000000

extern uint16_t windowWidth;
extern uint16_t windowHeight;

extern void setCloseOperation(void (*operation)) __attribute__((nonnull));
extern void setWindowResizeOperation(void (*operation) (const uint16_t width, const uint16_t height)) __attribute__((nonnull));
extern void setWindowMoveOperation(void (*operation) (const uint16_t posX, const uint16_t posY)) __attribute__((nonnull));
extern void setButtonPressOperation(void (*operation) (const uint16_t button, const uint16_t x, const uint16_t y)) __attribute__((nonnull));
extern void setButtonReleaseOperation(void (*operation) (const uint16_t button, const uint16_t x, const uint16_t y)) __attribute__((nonnull));
extern void setPointerMoveOperation(void (*operation) (const uint16_t x, const uint16_t y)) __attribute__((nonnull));
extern void setFocusGainedOperation(void (*operation) (void)) __attribute__((nonnull));
extern void setFocusLostOperation(void (*operation) (void)) __attribute__((nonnull));
extern void windowSetPointerPosition(const uint16_t x, const uint16_t y);
extern void windowCapturePointer(void);
extern void windowReleasePointer(void);
extern void windowHideCursor(void);
extern void windowShowCursor(void);
extern void windowRaise(void);
extern void windowSetFullscreen(const uint8_t to);
extern uint32_t pixelFromRGB(const uint8_t red, const uint8_t green, const uint8_t blue);
extern void windowCreate(uint16_t posX, uint16_t posY, const uint16_t width, const uint16_t height, const char *title, const uint8_t iconDimensions, const char *icon);
extern void windowClose(void);
extern void windowDrawFrame(void);
extern void windowPutPixel(const uint16_t x, const uint16_t y, const uint32_t color);
extern void windowFinishedLoop(void); //No more window operations are being sent, window can free data if necessary

#if defined(__unix__)
#define BUTTON_MOUSE_1 0x01 //Left
#define BUTTON_MOUSE_2 0x03 //Right
#define BUTTON_MOUSE_3 0x02 //Middle/scroll wheel
#define BUTTON_MOUSE_4 0x06
#define BUTTON_MOUSE_5 0x07
#define BUTTON_MOUSE_SCROLL_UP 0x04
#define BUTTON_MOUSE_SCROLL_DOWN 0x05
#define BUTTON_KEY_F1 0x43
#define BUTTON_KEY_F2 0x44
#define BUTTON_KEY_F3 0x45
#define BUTTON_KEY_F4 0x46
#define BUTTON_KEY_F5 0x47
#define BUTTON_KEY_F6 0x48
#define BUTTON_KEY_F7 0x49
#define BUTTON_KEY_F8 0x4a
#define BUTTON_KEY_F9 0x4b
#define BUTTON_KEY_F10 0x4c
#define BUTTON_KEY_F11 0x5f
#define BUTTON_KEY_F12 0x60
#define BUTTON_KEY_CONTROL_LEFT 0x25
#define BUTTON_KEY_CONTROL_RIGHT 0x69
#define BUTTON_KEY_ALT_LEFT 0x40
#define BUTTON_KEY_ALT_RIGHT 0x6c
#define BUTTON_KEY_SHIFT_LEFT 0x32
#define BUTTON_KEY_SHIFT_RIGHT 0x4e
#define BUTTON_KEY_ENTER 0x24
#define BUTTON_KEY_BACKSPACE 0x16
#define BUTTON_KEY_DELETE 0x77
/*#define BUTTON_KEY_CLEAR 0x0b //FIXME
#define BUTTON_KEY_CANCEL 0x69*/
#define BUTTON_KEY_ESCAPE 0x09
#define BUTTON_KEY_INSERT 0x76
/*#define BUTTON_KEY_SELECT 0x60 //FIXME
#define BUTTON_KEY_PRINT 0x61
#define BUTTON_KEY_HELP 0x6a
#define BUTTON_KEY_EXECUTE 0x62*/
#define BUTTON_KEY_LOCK_CAPS 0x42
#define BUTTON_KEY_LOCK_NUMBER 0x4d
#define BUTTON_KEY_LOCK_SCROLL 0x4e
#define BUTTON_KEY_ARROW_UP 0x6f
#define BUTTON_KEY_ARROW_DOWN 0x74
#define BUTTON_KEY_ARROW_LEFT 0x71
#define BUTTON_KEY_ARROW_RIGHT 0x72
#define BUTTON_KEY_PAGE_UP 0x70
#define BUTTON_KEY_PAGE_DOWN 0x75
#define BUTTON_KEY_HOME 06e
#define BUTTON_KEY_END 0x73
#define BUTTON_KEY_SPACE 0x41
#define BUTTON_KEY_TAB 0x17
#define BUTTON_KEY_GRAVE_TILDE 0x31
#define BUTTON_KEY_PLUS_EQUALS 0x15
#define BUTTON_KEY_MINUS_UNDERSCORE 0x14
#define BUTTON_KEY_SLASH_QUESTION 0x3d
#define BUTTON_KEY_BACKSLASH_PIPE 0x33
#define BUTTON_KEY_PERIOD_GREATER 0x3c
#define BUTTON_KEY_COMMA_LESS 0x3b
#define BUTTON_KEY_SEMICOLON_COLON 0x2f
#define BUTTON_KEY_QUOTE 0x30
#define BUTTON_KEY_BRACKET_OPEN 0x22
#define BUTTON_KEY_BRACKET_CLOSE 0x23
#define BUTTON_KEY_1_EXCLAIM 0x0a
#define BUTTON_KEY_2_AT 0x0b
#define BUTTON_KEY_3_HASHTAG 0x0c
#define BUTTON_KEY_4_DOLLAR 0x0d
#define BUTTON_KEY_5_PERCENT 0x0e
#define BUTTON_KEY_6_CARET 0x0f
#define BUTTON_KEY_7_AND 0x10
#define BUTTON_KEY_8_ASTERISK 0x11
#define BUTTON_KEY_9_PARENTHESIS_OPEN 0x12
#define BUTTON_KEY_0_PARENTHESIS_CLOSE 0x13
#define BUTTON_KEY_NUMPAD_ADD 0x56
#define BUTTON_KEY_NUMPAD_SUBTRACT 0x52
#define BUTTON_KEY_NUMPAD_MULTIPLY 0x3f
#define BUTTON_KEY_NUMPAD_DIVIDE 0x6a
#define BUTTON_KEY_NUMPAD_DECIMAL 0x5b
#define BUTTON_KEY_NUMPAD_SEPERATOR 0x00 //FIXME
#define BUTTON_KEY_NUMPAD_1 0x57
#define BUTTON_KEY_NUMPAD_2 0x58
#define BUTTON_KEY_NUMPAD_3 0x59
#define BUTTON_KEY_NUMPAD_4 0x53
#define BUTTON_KEY_NUMPAD_5 0x54
#define BUTTON_KEY_NUMPAD_6 0x55
#define BUTTON_KEY_NUMPAD_7 0x4f
#define BUTTON_KEY_NUMPAD_8 0x50
#define BUTTON_KEY_NUMPAD_9 0x51
#define BUTTON_KEY_NUMPAD_0 0x5a
#define BUTTON_KEY_A 0x26
#define BUTTON_KEY_B 0x38
#define BUTTON_KEY_C 0x36
#define BUTTON_KEY_D 0x28
#define BUTTON_KEY_E 0x1a
#define BUTTON_KEY_F 0x29
#define BUTTON_KEY_G 0x2a
#define BUTTON_KEY_H 0x2b
#define BUTTON_KEY_I 0x1f
#define BUTTON_KEY_J 0x2c
#define BUTTON_KEY_K 0x2d
#define BUTTON_KEY_L 0x2e
#define BUTTON_KEY_M 0x3a
#define BUTTON_KEY_N 0x39
#define BUTTON_KEY_O 0x20
#define BUTTON_KEY_P 0x21
#define BUTTON_KEY_Q 0x18
#define BUTTON_KEY_R 0x1b
#define BUTTON_KEY_S 0x27
#define BUTTON_KEY_T 0x1c
#define BUTTON_KEY_U 0x1e
#define BUTTON_KEY_V 0x37
#define BUTTON_KEY_W 0x19
#define BUTTON_KEY_X 0x35
#define BUTTON_KEY_Y 0x1d
#define BUTTON_KEY_Z 0x34

#elif defined(_WIN32)
#define BUTTON_MOUSE_1 0x01 //Left
#define BUTTON_MOUSE_2 0x02 //Right
#define BUTTON_MOUSE_3 0x04 //Middle/scroll wheel
#define BUTTON_MOUSE_4 0x05
#define BUTTON_MOUSE_5 0x06
//#define BUTTON_MOUSE_SCROLL_UP
//#define BUTTON_MOUSE_SCROLL_DOWN
#define BUTTON_KEY_F1 0x70
#define BUTTON_KEY_F2 0x71
#define BUTTON_KEY_F3 0x72
#define BUTTON_KEY_F4 0x73
#define BUTTON_KEY_F5 0x74
#define BUTTON_KEY_F6 0x75
#define BUTTON_KEY_F7 0x76
#define BUTTON_KEY_F8 0x77
#define BUTTON_KEY_F9 0x78
#define BUTTON_KEY_F10 0x79
#define BUTTON_KEY_F11 0x7a
#define BUTTON_KEY_F12 0x7b
#define BUTTON_KEY_CONTROL_LEFT 0xa2
#define BUTTON_KEY_CONTROL_RIGHT 0xa3
#define BUTTON_KEY_ALT_LEFT 0xa4
#define BUTTON_KEY_ALT_RIGHT 0xa5
#define BUTTON_KEY_SHIFT_LEFT 0xa0
#define BUTTON_KEY_SHIFT_RIGHT 0xa1
#define BUTTON_KEY_ENTER 0x0d
#define BUTTON_KEY_BACKSPACE 0x08
#define BUTTON_KEY_DELETE 0x2e
#define BUTTON_KEY_CLEAR 0x0c
#define BUTTON_KEY_CANCEL 0x03
#define BUTTON_KEY_ESCAPE 0x1b
#define BUTTON_KEY_INSERT 0x2d
#define BUTTON_KEY_SELECT 0x29
#define BUTTON_KEY_PRINT 0x2a
#define BUTTON_KEY_HELP 0x2f
#define BUTTON_KEY_EXECUTE 0x2b
#define BUTTON_KEY_LOCK_CAPS 0x14
#define BUTTON_KEY_LOCK_NUMBER 0x90
#define BUTTON_KEY_LOCK_SCROLL 0x91
#define BUTTON_KEY_ARROW_UP 0x26
#define BUTTON_KEY_ARROW_DOWN 0x28
#define BUTTON_KEY_ARROW_LEFT 0x25
#define BUTTON_KEY_ARROW_RIGHT 0x27
#define BUTTON_KEY_PAGE_UP 0x21
#define BUTTON_KEY_PAGE_DOWN 0x22
#define BUTTON_KEY_HOME 0x24
#define BUTTON_KEY_END 0x23
#define BUTTON_KEY_SPACE 0x20
#define BUTTON_KEY_TAB 0x09
#define BUTTON_KEY_GRAVE_TILDE 0xc0
#define BUTTON_KEY_PLUS_EQUALS 0xbb
#define BUTTON_KEY_MINUS_UNDERSCORE 0xbd
#define BUTTON_KEY_SLASH_QUESTION 0xbf
#define BUTTON_KEY_BACKSLASH_PIPE 0xdc
#define BUTTON_KEY_PERIOD_GREATER 0xbe
#define BUTTON_KEY_COMMA_LESS 0xbc
#define BUTTON_KEY_SEMICOLON_COLON 0xba
#define BUTTON_KEY_QUOTE 0xde
#define BUTTON_KEY_BRACKET_OPEN 0xdb
#define BUTTON_KEY_BRACKET_CLOSE 0xdd
#define BUTTON_KEY_1_EXCLAIM 0x31
#define BUTTON_KEY_2_AT 0x32
#define BUTTON_KEY_3_HASHTAG 0x33
#define BUTTON_KEY_4_DOLLAR 0x34
#define BUTTON_KEY_5_PERCENT 0x35
#define BUTTON_KEY_6_CARET 0x36
#define BUTTON_KEY_7_AND 0x37
#define BUTTON_KEY_8_ASTERISK 0x38
#define BUTTON_KEY_9_PARENTHESIS_OPEN 0x39
#define BUTTON_KEY_0_PARENTHESIS_CLOSE 0x30
#define BUTTON_KEY_NUMPAD_ADD 0x6b
#define BUTTON_KEY_NUMPAD_SUBTRACT 0x6d
#define BUTTON_KEY_NUMPAD_MULTIPLY 0x6a
#define BUTTON_KEY_NUMPAD_DIVIDE 0x6f
#define BUTTON_KEY_NUMPAD_DECIMAL 0x6e
#define BUTTON_KEY_NUMPAD_SEPERATOR 0x6c
#define BUTTON_KEY_NUMPAD_1 0x61
#define BUTTON_KEY_NUMPAD_2 0x62
#define BUTTON_KEY_NUMPAD_3 0x63
#define BUTTON_KEY_NUMPAD_4 0x64
#define BUTTON_KEY_NUMPAD_5 0x65
#define BUTTON_KEY_NUMPAD_6 0x66
#define BUTTON_KEY_NUMPAD_7 0x67
#define BUTTON_KEY_NUMPAD_8 0x68
#define BUTTON_KEY_NUMPAD_9 0x69
#define BUTTON_KEY_NUMPAD_0 0x60
#define BUTTON_KEY_A 0x41
#define BUTTON_KEY_B 0x42
#define BUTTON_KEY_C 0x43
#define BUTTON_KEY_D 0x44
#define BUTTON_KEY_E 0x45
#define BUTTON_KEY_F 0x46
#define BUTTON_KEY_G 0x47
#define BUTTON_KEY_H 0x48
#define BUTTON_KEY_I 0x49
#define BUTTON_KEY_J 0x4a
#define BUTTON_KEY_K 0x4b
#define BUTTON_KEY_L 0x4c
#define BUTTON_KEY_M 0x4d
#define BUTTON_KEY_N 0x4e
#define BUTTON_KEY_O 0x4f
#define BUTTON_KEY_P 0x50
#define BUTTON_KEY_Q 0x51
#define BUTTON_KEY_R 0x52
#define BUTTON_KEY_S 0x53
#define BUTTON_KEY_T 0x54
#define BUTTON_KEY_U 0x55
#define BUTTON_KEY_V 0x56
#define BUTTON_KEY_W 0x57
#define BUTTON_KEY_X 0x58
#define BUTTON_KEY_Y 0x59
#define BUTTON_KEY_Z 0x5a
#endif

#endif
