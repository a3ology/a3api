#ifndef _AERRORMANAGER_H
#define _AERRORMANAGER_H
#include "a3Global.h"
#include "MathManager.h"

extern void initCrashHandler(void (*exitHandlerFunction) (void)) __attribute__((nonnull));
extern char *getStackTrace(const uint8_t count);
extern char *getError(const int32_t errorCode); //Make sure to add 0xff to SIG errors!

#endif
